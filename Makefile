v = $(shell cat ./"VERSION")

.PHONY: all

all: package

package: independentresearch-xattr.tgz

independentresearch-xattr.tgz: xattr.so
	snow-chibi package "independentresearch/xattr.sld"
xattr.so:
	chibi-ffi independentresearch/xattr.stub independentresearch/xattr.c
	$(CC) -fPIC -shared -lchibi-scheme -o independentresearch/xattr.so independentresearch/xattr.c

clean:
	rm -rf "*.tgz"
	rm -rf "independentresearch/xattr.so" "independentresearch/xattr.c"

submit: package

	snow-chibi upload "independentresearch-xattr-$v.tgz"

